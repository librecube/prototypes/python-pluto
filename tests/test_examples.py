from pathlib import Path

import pytest

from pluto_parser import parse, pluto_parse


EXAMPLES_FOLDER = "examples"
EXAMPLE_DIR = Path(__file__).parent.parent / EXAMPLES_FOLDER
EXAMPLES = sorted(
    [e.relative_to(EXAMPLE_DIR) for e in EXAMPLE_DIR.glob("*.pluto")]
)


@pytest.mark.parametrize("example", EXAMPLES, ids=str)
def test_examples_parse(example):
    """The available pluto examples parse successfully.

    Note that this does not mean that the parse or source is correct."""
    with open(EXAMPLE_DIR / (str(example))) as f:
        contents = f.read()
    print("PLUTO script:")
    print(contents)
    tree = parse(contents)


@pytest.mark.parametrize("example", EXAMPLES, ids=str)
def test_examples_generate_reference(example):
    """The available pluto examples and Python reference are in sync."""
    pluto_file = Path(EXAMPLE_DIR / example.with_suffix(".pluto"))
    py_file = Path(EXAMPLE_DIR / example.with_suffix(".py"))
    if py_file.stem == "watchdog_body":
        pytest.xfail("WIP: Transformer methods incomplete")
    if not py_file.exists():
        pytest.skip("Reference for example {} not found.".format(example))
    with open(pluto_file) as f:
        pluto_script = f.read()
    with open(py_file) as f:
        py_script = f.read()
    generated_script, tree = pluto_parse(
        pluto_script, procedure_name=str(example.stem), debug=True
    )
    print(tree.pretty())
    assert str(generated_script) == py_script
